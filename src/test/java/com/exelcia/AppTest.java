package com.exelcia;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.Ignore;

/** Test Selenium **/
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import java.time.Duration;
import org.openqa.selenium.chrome.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testingAdditionSimple(){
        Integer result = App.additionner(2,3);
        assertTrue(result == 5);
    }

    @Test
    public void testingAdditionComplexe(){
        Integer result = App.additionner(2,3);
        Integer result2 = App.additionner(result,3);
        assertTrue(result2 == 8);
    }

    @Ignore
    @Test
    public void testingAdditionEchec(){
        Integer result = App.additionner(2,3);
        assertTrue(result == 8);
    }

    @Test
    public void testSeleniumChrome(){
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");

        ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
		options.addArguments("--no-sandbox"); // Bypass OS security model
		options.addArguments("--disable-extensions"); // disabling extensions
        options.addArguments("--headless"); // Headless
        
        
        WebDriver driver = new ChromeDriver(options);
        
        WebDriverWait wait = new WebDriverWait(driver, 10);
        
        try {
            driver.get("https://google.com/ncr");
            driver.findElement(By.name("q")).sendKeys("cheese" + Keys.ENTER);
            WebElement firstResult = wait.until(presenceOfElementLocated(By.cssSelector("h3")));
            System.out.println(firstResult.getAttribute("textContent"));
        } finally {
            driver.quit();
        }
    }

}
